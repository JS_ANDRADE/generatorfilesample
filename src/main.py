# -*- coding: utf-8 -*-
#######################################
# AUTHOR: JS                          #
# EMAIL: jsandrde1998@outlook.com     #
# License: GNU V3                     #
#######################################
from pathlib import Path

from PyQt5 import QtWidgets

import sys

from controller.DefaultController import DefaultController
from model.GenerateFile import GenerateFile
from view.widgets.MainWindow import MainWindow, QtGui

import rstr


def except_hook(cls, exception, traceback):
    sys.__excepthook__(cls, exception, traceback)


if __name__ == "__main__":

    sys.excepthook = except_hook


    # START MAIN WINDOW
    app = QtWidgets.QApplication([])
    view = MainWindow()
    view.show()

    # Enlace MVC
    controller = DefaultController()
    model = GenerateFile()

    view.setController(controller)

    controller.setView(view)
    controller.setModel(model)

    model.setController(controller)

    # EJecucion de app
    sys.exit(app.exec_())





