import random
import time

import rstr

class GenerateFile():

    _controller = None

    def __init__(self):
        pass

    def setController(self, controller):
        self._controller = controller

    def generateSample(self, data):

        file_sample = data['file_sample']
        separator_file = data['separator_file']
        delimiter_file = data['delimiter_file']
        is_header = data['is_header']
        num_rows = int(data['num_rows'])
        fields_data = data['fields_data']

        file = open(file_sample, 'w')

        if is_header == True:
            flag_init=0

            for field in fields_data:
                if flag_init==0:
                    file.write(field[0])
                    flag_init=1
                else:
                    file.write(delimiter_file+field[0])

            file.write("\n")

        

        progress = 1
        porcent = num_rows / 100

        row = 1

        while row <= num_rows:

            #init_for = time.time()

            flag_init=0


            for field in fields_data:

                type = field[1]
                min_length = field[2]
                max_length = field[3]
                value = field[4]
                value_aux = None

                if type == "VALUE":
                    value_aux = value
                elif type == "LIST":
                    list = value.split(";")
                    len_list = len(list)
                    value_aux = list[random.randint(0, len_list-1)]
                elif type == "NUMERIC":
                    value_aux = rstr.xeger('\d{' + min_length + ','+ max_length + '}')
                elif type == "ALPHANUMERIC":
                    value_aux = rstr.xeger('[0-9a-zA-Z]{' + min_length + ','+ max_length + '}')
                elif type == "LETTERS":
                    value_aux = rstr.xeger('[a-zA-Z]{' + min_length + ','+ max_length + '}')
                elif type == "PATTERN":
                    value_aux = rstr.xeger(''+ value +'')
                elif type == "NULL":
                    value_aux = ""
                elif type == "FLOAT":
                    if value == "0":
                        value_aux = rstr.xeger('\d{' + min_length + ','+ max_length + '}')
                    else:
                        value_aux = rstr.xeger('\d{' + min_length + ','+ max_length + '}\.\d{' + value + '}')
                else:
                    raise TypeError("ERROR: TIPO " + type + " NO SE ENCUENTRA IMPLEMENTADO")

                if separator_file == 'FIXED':
                    
                    if type == "FLOAT" and value != "0":
                        max_length = int(max_length) + int(value) + 1
                    
                    value_aux = value_aux.ljust(int(max_length))
            

                if flag_init == 0:
                    file.write(value_aux)
                    flag_init=1
                else:
                    file.write(delimiter_file + value_aux)

            file.write("\n")

            if (num_rows > 100):
                if porcent<=row:
                    self._controller.updateProgress(progress)
                    progress += 1
                    porcent += num_rows / 100

                if row == num_rows:
                    self._controller.updateProgress(100)
            else:
                self._controller.updateProgress(row)

            row += 1

        file.close()





