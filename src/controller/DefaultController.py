


class DefaultController():

    _view = None
    _model = None

    def __init__(self):
        pass

    def setModel(self, model):
        self._model = model

    def setView(self, view):
        self._view = view


    def createFileSample(self, data):

        path_directory = data['path_directory']
        name_file = data['name_file']
        extension_file = data['extension_file']

        file_sample = path_directory + "/" + name_file + "." + extension_file.lower()

        data_aux = {
            'file_sample': file_sample,
            'separator_file' : data['separator_file'],
            'delimiter_file': data['delimiter_file'],
            'is_header': data['is_header'],
            'num_rows': data['num_rows'],
            'fields_data': data['fields_data']
        }

        self._model.generateSample(data_aux)


    def updateProgress(self, value):
        self._view.worker.progress.emit(value)

