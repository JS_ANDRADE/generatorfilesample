import os
import re
import threading
import time
from pathlib import Path
import traceback
import json

from PyQt5.QtCore import pyqtSignal, QThread, QObject, QDir
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QTableWidgetItem, QFileDialog

from view.ui.MainUi import *



class MainWindow(QtWidgets.QMainWindow, Ui_MainWindow):

    _controller = None
    _model = None

    TYPES = {"VALUE", "NUMERIC", "LETTERS", "ALPHANUMERIC", "PATTERN", "LIST", "NULL", "FLOAT"}

    def __init__(self, *args, **kwargs):
        QtWidgets.QMainWindow.__init__(self, *args, **kwargs)
        self.setupUi(self)
        self.initComponents()

        #self.preloadData()

    def setController(self, controller):
        self._controller = controller

    def initComponents(self):
        self.connectSignals()

    #Connexion de slots
    def connectSignals(self):

        self.btSearchDirectory.clicked.connect(self.actionSearchDirectory)

        self.cbSeparator.currentIndexChanged.connect(self.actionChangeSeparator)

        self.btChargerSchema.clicked.connect(self.actionChargerSchema)

        self.btChargerTemplate.clicked.connect(self.actionChargerTemplate)
        self.btSaveTemplate.clicked.connect(self.actionSaveTemplate)

        self.btAddField.clicked.connect(self.actionAddField)
        self.btDeleteField.clicked.connect(self.actionDeleteFields)

        self.btUpField.clicked.connect(self.actionMoveUpField)
        self.btDownField.clicked.connect(self.actionMoveDownField)

        self.btGenerateFile.clicked.connect(self.actionGenerateFile)

    def actionSearchDirectory(self):
        directory = str(QtWidgets.QFileDialog.getExistingDirectory(self, "Seleciona directorio:"))

        if directory:
            self.lePathDirectory.setText(directory)

    def actionChangeSeparator(self):
        
        separator = self.cbSeparator.currentText()

        if separator == 'FIXED':
            self.cbDelimiters.setCurrentIndex(0)
            self.cbDelimiters.setEnabled(False)
        elif separator == 'DELIMITER':
            self.cbDelimiters.setEnabled(True)
        else:
            self.cbDelimiters.setEnabled(True)

    def actionChargerSchema(self):
        file, _ = QFileDialog.getOpenFileName(self, 'Buscar Archivo', QDir.homePath(),
                                              "Text Files (*.json)")
        if file:
            try:
                self.chargerSchema(file)
                self.showMessage("Se cargado correctamente la plantilla de " + file)
            except Exception as e:
                self.showError(str(e))

    def actionChargerTemplate(self):
        file, _ = QFileDialog.getOpenFileName(self, 'Buscar Archivo', QDir.homePath(),
                                              "Text Files (*.txt)")
        if file:
            try:
                self.chargerTemplate(file)
                self.showMessage("Se cargado correctamente la plantilla de " + file)
            except Exception as e:
                self.showError(str(e))


    def actionSaveTemplate(self):
        directory = str(QtWidgets.QFileDialog.getExistingDirectory(self, "Seleciona directorio:"))

        if directory:
            msg_validate_table = self.validateFieldsTable()

            if msg_validate_table == "":
                try:
                    self.saveTemplate(directory)
                    self.showMessage("Se ha guardado correctamente la plantilla en " + directory)
                except Exception as e:
                    self.showError(str(e))
            else:
                self.showWarning(msg_validate_table)





    def actionAddField(self):
        rowPosition = self.tblFields.rowCount()

        self.tblFields.insertRow(rowPosition)

        comboTypes = QtWidgets.QComboBox()
        comboTypes.addItems(sorted(self.TYPES))
        self.tblFields.setCellWidget(rowPosition,1, comboTypes)

    def actionDeleteFields(self):
        self.tblFields.removeRow(self.tblFields.currentRow())

    def actionGenerateFile(self):

        if (self.validateData()):

            try:
                data = self.getData()

                if int(self.leNumRows.text()) <= 100:
                    self.pbCompleted.setMaximum(int(self.leNumRows.text()))

                

                # Inciamos el worker
                self.worker = WorkerProgress(self._controller, data)
                # Iniciamos el hilo
                self.thread = QThread()
                # Movemos el worker al hilo nuevo
                self.worker.moveToThread(self.thread)
                # Conectamos los signals y los slots
                self.thread.started.connect(self.worker.run)
                self.worker.finished.connect(self.thread.quit)
                self.worker.finished.connect(self.worker.deleteLater)
                self.thread.finished.connect(self.thread.deleteLater)
                self.worker.progress.connect(self.updateProgressBar)
                self.worker.error.connect(self.errorThread)
                # Inicamos el hilo
                self.thread.start()

                #Fin del hilo
                self.btGenerateFile.setEnabled(False)
                self.thread.finished.connect(
                    lambda : self.btGenerateFile.setEnabled(True)
                )
                self.worker.finished.connect(
                    lambda : self.showMessage("SE HA CREADO EL FICHERO DE EJEMPLO CORRECTAMENTE")
                )
                self.thread.finished.connect(
                    lambda : self.pbCompleted.setValue(0)
                )

            except Exception as e:
                traceback.print_exc()
                self.showError(str(e))
                
    def deleteAllTable(self):
        row_count = self.tblFields.rowCount()
        i=row_count-1
        while i>=0:
            self.tblFields.removeRow(i)
            i -= 1


    def chargerSchema(self, file):
        
        self.deleteAllTable()



        with open(file) as file:
            data = json.load(file)

            for field in data['fields']:
                
                name = ""
                type = ""
                min_length = ""
                max_length = ""
                value = ""

                if field['metadata'] == False:
                    rowPosition = self.tblFields.rowCount()

                    self.tblFields.insertRow(rowPosition)

                    comboTypes = QtWidgets.QComboBox()
                    comboTypes.addItems(sorted(self.TYPES))
                    self.tblFields.setCellWidget(rowPosition, 1, comboTypes)


                    name = field['name']

                    if  "ALPHANUMERIC" in field['logicalFormat']:
                        type = 'ALPHANUMERIC'
                    elif "DECIMAL" in field['logicalFormat']:
                        type = 'FLOAT'
                        value = '0'
                    elif "TIMESTAMP" in field['logicalFormat']:
                        type = 'ALPHANUMERIC'
                    elif "DATE" in field['logicalFormat']:
                        type = 'ALPHANUMERIC'
                    elif "NUMERIC" in field['logicalFormat']:
                        type = 'NUMERIC'
                    
                    try:
                        min_length = re.findall(r'\d+', field['logicalFormat'])[0]
                        max_length = re.findall(r'\d+', field['logicalFormat'])[0]
                    except Exception:
                        min_length = "10"
                        max_length = "10"

                
                    self.tblFields.setItem(rowPosition, 0, QTableWidgetItem(name))

                    self.tblFields.cellWidget(rowPosition, 1).setCurrentText(type)
                    self.tblFields.setItem(rowPosition, 2, QTableWidgetItem(min_length))
                    self.tblFields.setItem(rowPosition, 3, QTableWidgetItem(max_length))
                    self.tblFields.setItem(rowPosition, 4, QTableWidgetItem(value))

    def chargerTemplate(self, file):
        f = open(file)

        name_fields = []
        types_fields = []
        min_length_fields = []
        max_length_fields = []
        values_fields = []


        for line in f:
            line_split = line.split("|")

            if len(line_split) != 5:
                raise ValueError("El formato del fichero " + file + " no es correcto, corrigalo e intentelo de nuevo")

            name_field = line_split[0].strip()
            type = line_split[1].strip()
            try:
                min_length = int(line_split[2].strip())
                max_length = int(line_split[3].strip())
            except ValueError:
                raise ValueError("El formato del fichero " + file + " no es correcto, corrigalo e intentelo de nuevo")
            value = line_split[2].strip()

            if type not in self.TYPES:
                raise ValueError("El tipo campo " + type + " de la linea " + line + " no es valido")

            name_fields.append(name_field)
            types_fields.append(type)
            min_length_fields.append(min_length)
            max_length_fields.append(max_length)
            values_fields.append(value)

        self.deleteAllTable()

        for i in range(len(name_fields)):

            rowPosition = self.tblFields.rowCount()

            self.tblFields.insertRow(rowPosition)

            comboTypes = QtWidgets.QComboBox()
            comboTypes.addItems(sorted(self.TYPES))
            self.tblFields.setCellWidget(rowPosition, 1, comboTypes)

            self.tblFields.setItem(rowPosition, 0, QTableWidgetItem(name_fields[i]))
            self.tblFields.cellWidget(rowPosition, 1).setCurrentText(types_fields[i])
            self.tblFields.setItem(rowPosition, 2, QTableWidgetItem(min_length_fields[i]))
            self.tblFields.setItem(rowPosition, 3, QTableWidgetItem(max_length_fields[i]))
            self.tblFields.setItem(rowPosition, 4, QTableWidgetItem(values_fields[i]))


        f.close()

    def saveTemplate(self, directory):
        file = directory + "\\" + "template_GFS.txt"

        data_table = self.getDataOfTable()
        f = open(file, "w")

        for row in data_table:
            name_field = row[0]
            type = row[1]
            min_length = row[2]
            max_length = row[3]
            value = row[4]
            f.write(name_field.strip() + "|" + type.strip() + "|" + \
                min_length.strip() + "|" + max_length.strip() + "|" + value.strip() + "\n")

        f.close()

    def updateProgressBar(self, value):
        self.pbCompleted.setValue(int(value))

    def errorThread(self, msg):
        self.showError(msg)

        self.thread.quit()
        self.thread.wait()

    def validateData(self):
        msg_error = ""

        if not self.lePathDirectory.text():
            msg_error += "Introduza un directorio por favor\n"
        else:
            if not os.path.isdir(self.lePathDirectory.text()):
                msg_error += "El directorio introducido no es valido\n"

        if not self.leFileName.text().strip():
            msg_error += "Introduza un nombre para el fichero por favor\n"

        if not self.leNumRows.text():
            msg_error += "Introduza un numero de registros que necesita para la sample\n"
        else:
            if not self.leNumRows.text().isdigit():
                msg_error += "Introduza un numero de registros numerico para la sample\n"
            else:
                if int(self.leNumRows.text()) <= 0:
                    msg_error += "Introduza un numero de registros superior a 0\n"

        msg_error_fields_table = self.validateFieldsTable()

        if msg_error_fields_table != "":
            msg_error += msg_error_fields_table


        if msg_error == "":

            return True

        else:
            self.showWarning(msg_error)
            return False

    def validateFieldsTable(self):

        msg_error = ""

        data_table = self.getDataOfTable()

        if data_table == None or len(data_table) <= 0:
            msg_error += "No hay campos para generar\n"
        else:

            position = 1

            for row in data_table:

                column = row[0]
                type = row[1]    
                min_length = row[2]
                max_length = row[3]
                value = row[4]

                #{"VALUE", "NUMERIC", "LETTERS", "ALPHANUMERIC", "PATTERN", "LIST", "NULL"}
                if column == None or column == "":
                    msg_error += "El nombre del campo esta vacio en la fila numero: " + str(
                        position) + "\n"
                elif (min_length == None or min_length == "") or (max_length == None or max_length == ""):
                    msg_error += "Falta por definir el tamaño del campo en la fila numero: " + str(
                        position) + "\n"
                elif (value == None or value == "") \
                    and type in ['LIST', 'PATTERN', 'VALUE', 'FLOAT']:
                    msg_error += "Falta por definir un valor en la fila numero: " + str(
                        position) + "\n"
                elif min_length != None and max_length != None:
                    try:
                        int(min_length)
                        int(max_length)
                    except ValueError:
                        msg_error += "Introduzca un valor valido para el tamaño del campo en la fila numero: " + str(
                        position) + "\n"
                

                position += 1

        return msg_error

    def showMessage(self, msg):
        msgBox = QtWidgets.QMessageBox()
        msgBox.setText(msg)
        msgBox.exec_()

    def showError(self, msg):
        msgBox = QtWidgets.QMessageBox()
        msgBox.setIcon(QtWidgets.QMessageBox.Critical)
        msgBox.setText(msg)
        msgBox.setWindowTitle("ERROR")
        msgBox.exec_()

    def showWarning(self, msg):
        msgBox = QtWidgets.QMessageBox()
        msgBox.setIcon(QtWidgets.QMessageBox.Warning)
        msgBox.setText(msg)
        msgBox.setWindowTitle("WARNING")
        msgBox.exec_()

    def getDataOfTable(self):
        data = []

        row_count = self.tblFields.rowCount()
        column_count = self.tblFields.columnCount()

        for row in range(row_count):
            row_data = []
            for column in range(column_count):
                cell_text = self.getTextOfItem(self.tblFields, row, column)
                cell_text = cell_text.strip()
                if (cell_text == None):
                    row_data.append(None)
                else:
                    row_data.append(cell_text)

            data.append(row_data)

        return data

    def getTextOfItem(self, item, row, column):

        object = item.cellWidget(row, column)

        if (object == None):
            object = item.item(row, column)



        if (isinstance(object, QtWidgets.QComboBox)):
            return object.currentText()
        if (isinstance(object, QtWidgets.QTableWidgetItem)):
            return object.text()

        return ""

    def getData(self):

        path_directory = self.lePathDirectory.text().strip()
        name_file = self.leFileName.text().strip()
        extension_file = self.cbExtensions.currentText().strip()
        separator_file = self.cbSeparator.currentText().strip()
        delimiter_file = self.cbDelimiters.currentText().strip()
        is_header = self.chbHeader.isChecked()
        num_rows = self.leNumRows.text().strip()
        fields_data = self.getDataOfTable()


        data = {
            'path_directory' : path_directory,
            'name_file' : name_file,
            'extension_file' : extension_file,
            'separator_file' : separator_file,
            'delimiter_file' : delimiter_file,
            'is_header' : is_header,
            'num_rows' : num_rows,
            'fields_data' : fields_data
        }

        return data

    def actionMoveDownField(self):
        row = self.tblFields.currentRow()
        column = self.tblFields.currentColumn();
        index_combo=self.tblFields.cellWidget(row, 1).currentIndex()
        if row < self.tblFields.rowCount() - 1:
            self.tblFields.insertRow(row + 2)

            for i in range(self.tblFields.columnCount()):

                self.tblFields.setItem(row + 2, i, self.tblFields.takeItem(row, i))

                if (i == 1):
                    comboTypes = QtWidgets.QComboBox()
                    comboTypes.addItems(sorted(self.TYPES))
                    self.tblFields.setCellWidget(row + 2, i, comboTypes)
                    self.tblFields.cellWidget(row + 2,i).setCurrentIndex(index_combo)

                self.tblFields.setCurrentCell(row + 2, column)

            self.tblFields.removeRow(row)


    def actionMoveUpField(self):
        row = self.tblFields.currentRow()
        column = self.tblFields.currentColumn()
        index_combo = self.tblFields.cellWidget(row, 1).currentIndex()
        if row > 0:
            self.tblFields.insertRow(row - 1)
            for i in range(self.tblFields.columnCount()):
                self.tblFields.setItem(row - 1, i, self.tblFields.takeItem(row + 1, i))

                if (i == 1):
                    comboTypes = QtWidgets.QComboBox()
                    comboTypes.addItems(sorted(self.TYPES))
                    self.tblFields.setCellWidget(row - 1, i, comboTypes)
                    self.tblFields.cellWidget(row - 1, i).setCurrentIndex(index_combo)

                self.tblFields.setCurrentCell(row - 1, column)
            self.tblFields.removeRow(row + 1)

    def setEnabledComponents(self, flag):

        self.lePathDirectory.setEnabled(flag)
        self.btSearchDirectory.setEnabled(flag)
        self.leFileName.setEnabled(flag)
        self.cbExtensions.setEnabled(flag)
        self.cbDelimiters.setEnabled(flag)
        self.chbHeader.setEnabled(flag)
        self.tblFields.setEnabled(flag)
        self.btUpField.setEnabled(flag)
        self.btDownField.setEnabled(flag)
        self.btAddField.setEnabled(flag)
        self.btDeleteField.setEnabled(flag)
        self.btGenerateFile.setEnabled(flag)
        self.leNumRows.setEnabled(flag)

    def preloadData(self):
            self.lePathDirectory.setText("C:/ficheros/prueba_file_sample")
            self.leFileName.setText("prueba")
            self.leNumRows.setText("100")

            self.cbSeparator.setCurrentIndex(1)
            self.cbDelimiters.setCurrentIndex(1)

            self.actionAddField()
            self.tblFields.setItem(0,0, QTableWidgetItem("A"))
            self.tblFields.setItem(0, 2, QTableWidgetItem("2"))
            self.tblFields.setItem(0, 3, QTableWidgetItem("4"))
            self.tblFields.cellWidget(0, 1).setCurrentIndex(0)

            self.actionAddField()
            self.tblFields.setItem(1,0, QTableWidgetItem("B"))
            self.tblFields.setItem(1, 2, QTableWidgetItem("2"))
            self.tblFields.setItem(1, 3, QTableWidgetItem("4"))
            self.tblFields.setItem(1, 4, QTableWidgetItem("4"))
            self.tblFields.cellWidget(1, 1).setCurrentIndex(1)



            self.actionAddField()
            self.tblFields.setItem(2, 0, QTableWidgetItem("C"))
            self.tblFields.setItem(2, 2, QTableWidgetItem("3"))
            self.tblFields.setItem(2, 3, QTableWidgetItem("5"))
            self.tblFields.cellWidget(2, 1).setCurrentIndex(2)

            self.actionAddField()
            self.tblFields.setItem(3, 0, QTableWidgetItem("D"))
            self.tblFields.setItem(3, 2, QTableWidgetItem("2"))
            self.tblFields.setItem(3, 3, QTableWidgetItem("4"))
            self.tblFields.setItem(3, 4, QTableWidgetItem("PRUEBA_LISTA1;PRUEBA_LISTA2;PRUEBA_LISTA3"))
            self.tblFields.cellWidget(3, 1).setCurrentIndex(3)

            self.actionAddField()
            self.tblFields.setItem(4, 0, QTableWidgetItem("E"))
            self.tblFields.setItem(4, 2, QTableWidgetItem("6"))
            self.tblFields.setItem(4, 3, QTableWidgetItem("8"))
            self.tblFields.cellWidget(4, 1).setCurrentIndex(4)
            
            self.actionAddField()
            self.tblFields.setItem(5, 0, QTableWidgetItem("F"))
            self.tblFields.setItem(5, 2, QTableWidgetItem("6"))
            self.tblFields.setItem(5, 3, QTableWidgetItem("8"))
            self.tblFields.cellWidget(5, 1).setCurrentIndex(5)

            self.actionAddField()
            self.tblFields.setItem(6, 0, QTableWidgetItem("G"))
            self.tblFields.setItem(6, 2, QTableWidgetItem("2"))
            self.tblFields.setItem(6, 3, QTableWidgetItem("4"))
            self.tblFields.setItem(6, 4, QTableWidgetItem("\d{1,5}"))
            self.tblFields.cellWidget(6, 1).setCurrentIndex(6)

            self.actionAddField()
            self.tblFields.setItem(7, 0, QTableWidgetItem("H"))
            self.tblFields.setItem(7, 2, QTableWidgetItem("1"))
            self.tblFields.setItem(7, 3, QTableWidgetItem("5"))
            self.tblFields.setItem(7, 4, QTableWidgetItem("PRUEBA_VALOR"))
            self.tblFields.cellWidget(7, 1).setCurrentIndex(7)


class WorkerProgress(QObject):

    error = pyqtSignal(str)
    finished = pyqtSignal()
    progress = pyqtSignal(int)

    _controller = None
    _data = None

    def __init__(self, controller, data):
        super(WorkerProgress, self).__init__()
        self._controller = controller
        self._data = data

    def run(self):
        try:
            self._controller.createFileSample(self._data)
            self.finished.emit()
        except Exception as e:
            self.error.emit(str(e))










